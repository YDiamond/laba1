//
// Created by diy on 25.08.2021.
//

#ifndef LABA1_PARTITION_H
#define LABA1_PARTITION_H

#include "stdio.h"
#include "string.h"
#include "dirent.h"
#include <stddef.h>
#include "stdio.h"
#include "string.h"
#include "limits.h"
#include <malloc.h>
#include <fcntl.h>
#include <linux/fs.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include "string_utils.h"


struct partition {
    char* d_name;
};

struct partitions{
    struct partition* mas;
    size_t size;
};

struct partitions* get_partitions();

#endif //LABA1_PARTITION_H

