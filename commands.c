//
// Created by diy on 03.04.2021.
//

#include "commands.h"


struct args quantity[] = {
        {PWD,     0},
        {LS,      0},
        {CD,      1},
        {CP,      2},
        {UNKNOWN, 0},
        {EXIT,    0}
};

struct intercept intercepts[] = {
        {PWD,  "pwd"},
        {LS,   "ls"},
        {CD,   "cd"},
        {CP,   "cp"},
        {EXIT, "exit"}
};

size_t size_coms = 5;


enum code get_code(char *code);

int get_arg(char *source, char **dest, char delim);

union command get(enum code, char **);

void free_args(char **args, int end) {
    for (int i = 0; i < end; i++) {
        free(args[i]);
    }
    free(args);
}

union command get_command(char *line) {
    char *trimmed_line;
    trim(line, &trimmed_line);

    if (strcmp(trimmed_line, "") == 0) {
        free(trimmed_line);
        return (union command) {UNKNOWN};
    }
    int space_found = 0;
    //get command
    while (trimmed_line[space_found] != ' ' && trimmed_line[space_found] != '\0') space_found++;
    char *substr = substring(trimmed_line, 0, space_found);
    enum code code = get_code(substr);
    free(substr);

    while (trimmed_line[space_found] == ' ') space_found++;

    int i = space_found;
    int size = quantity[code].size;
    int count = 0;
    char **args = malloc(sizeof(char *) * size);
    char *arg;
    while (trimmed_line[i] != '\0' && count < size) {
        if (trimmed_line[i] == '\"') {
            i++;
            i += get_arg(trimmed_line + i, &arg, '\"');
            args[count++] = arg;
            i++;
        } else if (trimmed_line[i] != ' ') {
            i += get_arg(trimmed_line + i, &arg, ' ');
            args[count++] = arg;
        }
        i++;
    }


    free(trimmed_line);
    return get(code, args);
}

int get_arg(char *source, char **dest, char delim) {
    int i = 0;
    while (source[i] != delim && source[i] != '\0') {
        i++;
    }
    *dest = substring(source, 0, i);
    return i;
}

void free_command(union command* command){
    switch (command->code) {
        case CD:
            free(command->arg.arg);
        case CP:
            free(command->arg2.arg);
            free(command->arg2.arg2);
    }
}

union command get(enum code code, char **args) {
    union command ret;
    switch (code) {
        case PWD:
        case EXIT:
        case LS:
            return (union command) {code};
        case CD:
            return (union command) {.arg = {code, args[0]}};
        case CP:
            return (union command) {.arg2 = {code, args[0], args[1]}};
        case UNKNOWN:
            return (union command) {code};
    }
}

enum code get_code(char *code) {
    for (int i = 0; i < size_coms; i++) {
        if (strcmp(code, intercepts[i].name) == 0)
            return intercepts[i].code;
    }
    return UNKNOWN;
}

const char *pwd(state_fs *stateFs, union command command) {
    if(command.code == PWD)
        return stateFs->dir_name;
    return NULL;
}

llist_node *ls(state_fs *stateFs, union command command) {
    if(command.code == LS)
        return stateFs->dir_names;
    return NULL;
}



int cd(state_fs** stateFs, union command command){
    if(command.code == CD)
        return true_cd(stateFs, command.arg.arg);
    return -1;
}

int cp(state_fs* stateFs, union command command){
    if(command.code == CP)
        return copy_by_chars(stateFs, command.arg2.arg, command.arg2.arg2);
    return -1;

}