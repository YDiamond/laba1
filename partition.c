//
// Created by diy on 25.08.2021.
//

#include "partition.h"

struct partitions* create_partitions(){
    struct partitions* partitions = malloc(sizeof(struct partitions));
    partitions->mas = NULL;
    partitions->size = 0;
    return partitions;
}

void add_partition(struct partitions* out, struct partition* partition){
    out->size += 1;
    out->mas = realloc(out->mas, (out->size) * sizeof (struct partition));
    out->mas[out->size - 1] = *partition;
}

struct partition*  create_partition(struct dirent* sys_sub_entry){
    struct partition * partition = malloc(sizeof(struct partition));
    partition->d_name = true_concat_strs("/dev/", sys_sub_entry->d_name);
    return partition;
}

struct partitions* get_partitions() {
    DIR* sys_dir = opendir("/sys/block/");
    if (sys_dir == NULL)
        return NULL;
    struct dirent* sys_entry;
    struct partitions* out = create_partitions();

    while ((sys_entry = readdir(sys_dir)) != NULL) {
        if (start_with(sys_entry->d_name, "sd")) {

            char* block = true_concat_strs("/sys/block/", sys_entry->d_name);

            DIR* sys_sub_dir = opendir(block);

            if (sys_sub_dir != NULL) {
                struct dirent* sys_sub_entry;

                while ((sys_sub_entry = readdir(sys_sub_dir)) != NULL) {
                    if (start_with(sys_sub_entry->d_name, "sd")) {
                        struct partition * partition = create_partition(sys_sub_entry);
                        add_partition(out, partition);
                    }
                }
            }

            free(block);
        }
    }

    return out;
}
