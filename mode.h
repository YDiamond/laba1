//
// Created by diy on 25.08.2021.
//

#ifndef LABA1_MODE_H
#define LABA1_MODE_H
#include "getopt.h"
#include "string.h"
#include "stdio.h"

enum modes {
    UNDEFINED = 0,
    PARTITIONS = 1,
    FS = 2
};

struct program {
    enum modes mode;
    char* path;
};

struct program get_args(const int argc, char** argv);

#endif //LABA1_MODE_H
