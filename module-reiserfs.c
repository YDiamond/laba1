//
// Created by diy on 07.03.2021.
//

#include "module-reiserfs.h"



int compare_short_keys(struct reiserfs_key const* a, struct reiserfs_key const* b)
{
    // compare 1. integer
    if( a->k2_dir_id < b->k2_dir_id )
        return -1;
    if( a->k2_dir_id > b->k2_dir_id )
        return 1;

    // compare 2. integer
    if( a->k2_objectid < b->k2_objectid )
        return -1;
    if( a->k2_objectid > b->k2_objectid )
        return 1;
    return 0;
}

int compare_long_keys(struct reiserfs_key const* a, struct reiserfs_key const* b)
{
    // compare 1. integer
    if( a->k2_dir_id < b->k2_dir_id )
        return -1;
    if( a->k2_dir_id > b->k2_dir_id )
        return 1;

    // compare 2. integer
    if( a->k2_objectid < b->k2_objectid )
        return -1;
    if( a->k2_objectid > b->k2_objectid )
        return 1;

//    // compare 3. integer
//    if( a->u. < b->k_offset )
//        return -1;
//    if( a->k_offset > b->k_offset )
//        return 1;
//
//    // compare 4. integer
//    if( a->k_type < b->k_type )
//        return -1;
//    if( a->k_type > b->k_type )
//        return 1;
    return 0;
}
