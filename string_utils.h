//
// Created by diy on 09.05.2021.
//

#ifndef LABA1_STRING_UTILS_H
#define LABA1_STRING_UTILS_H

#include <string.h>
#include <stdlib.h>
#include "math.h"


int is_ws(char c);
int trim(char* src, char ** dst);
char * copy_str(char* str);
char * substring(const char * str, int begin, int end); //   str[begin, end)
int start_with(char* src, char* substr);
char* true_concat_strs(char* source_1, char* source_2);
#endif //LABA1_STRING_UTILS_H


