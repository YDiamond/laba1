//
// Created by diy on 31.08.2021.
//

#include "user_interface.h"
#include "commands.h"
void println(char* str){
    printf("%s\n", str);
}

void exec_pwd(union command command, state_fs* state){
    char *string = pwd(state, command);
    if(string != NULL){
        println(string);
    }
}

void exec_ls(union command command, state_fs* state){
    llist_node *node = ls(state, command);
    if(node != NULL){
        foreach(node, println);
    }
}

void exec_cd(union command command, state_fs** state){
    int error = cd(state, command);
    if(error == NOT_DIR || error == NOT_FOUND){
        println("It is not directory");
    }
}

void exec_cp(union command command, state_fs* state){
    int error = cp(state, command);
    if(error == NOT_FOUND){
        println("Not found directory or file");
    }
}


void print_partitions(){
    struct partitions * partitions = get_partitions();
    for(int i = 0; i < partitions->size; i++ ){
        printf("%s\n", partitions->mas[i].d_name);
    }
}

void read_command(char * input, size_t max){
    printf(">>>");
    getline(&input, &max, stdin);
}



void execute_operation(union command command, state_fs** state){
    switch (command.code) {

        case PWD:
            exec_pwd(command, *state);
            break;
        case LS:
            exec_ls(command, *state);
            break;
        case CD:
            exec_cd(command, state);
            break;
        case CP:
            exec_cp(command, *state);
            break;
        case UNKNOWN:
            println(
                   "Unknown command\nls - get files from directory\ncp filename path - copy file or directory to path \npwd  - get filepath\ncd path - go to directory\nexit - exit from progrmam"
            );
            break;
        case EXIT:
            break;
    }
}

int entrance(char* path){
    state_fs ** state = malloc(sizeof(state_fs*));
    *state = malloc(sizeof (state_fs));
    int error = create_state(path, *state);
    if(error == ISNT_REISERFS)
        goto clean_state;


    size_t max = LINE_MAX;
    char input[LINE_MAX];
    read_command(input, max);
    union command command = get_command(input);

    while (command.code != EXIT){
        execute_operation(command, state);
        read_command(input, max);
        free_command(&command);
        command = get_command(input);
    }
    printf("Bye bye\n");
    return 0;

    clean_state:
        delete_state(*state);
        free(state);
    return 1;
}
