//
// Created by diy on 18.04.2021.
//
#include "module-reiserfs.h"
int isDir(struct item_head item){
    if (item.ih_format == KEY_FORMAT_1) {
        if(item.ih_key.u.k2_offset_v1.k_uniqueness == 500){
            return 1;
        }
        return 0;
    }else{
        if((item.ih_key.u.k2_offset_v2.v & 15) == 3){
            return 1;
        }
        return 0;
    }
}
int isStat(struct item_head item){
    if(item.ih_key.u.k2_offset_v2.v == 0)
        return 1;
    return 0;
}