//
// Created by YDiamond on 03.11.2020.
//

#ifndef LINKEDLIST_H
#define LINKEDLIST_H
#include <linux/types.h>


typedef struct llist_node {
    struct llist_node *next;
    struct llist_node *prev;
    int len;
    char* value;
    __le32 dir_id;
    __le32 object_id;
} llist_node;


llist_node* llist_create(char *str, int len, __le32 dir_id, __le32 object_id);
llist_node* list_add_front (llist_node** llist, char *str, int len, __le32 dir_id, __le32 object_id);
llist_node* list_add_back (llist_node** llist, char *str, int len, __le32 dir_id, __le32 object_id);
llist_node* llist_at(llist_node* llist, int at, int* error);
char * list_get (llist_node* list, int index, int* error);
int length(llist_node* llist);
void llist_free (llist_node** llist);
void foreach (llist_node* llist, void (*consumer)(char *));
struct reiserfs_key* find_key(struct llist_node* llist, const char* value);
llist_node * copy(llist_node* llist);
//llist_node* map(llist_node* llist, char* (*operator)(char *));
//llist_node* map_mut(llist_node** llist, char*(*operator)(char *));
//char* foldl(llist_node * llist, char *start_acc, char* (*operator)(char *, char *));


#endif //LINKEDLIST_H
