//
// Created by diy on 03.04.2021.
//

#include <malloc.h>
#include "state.h"
#include <string.h>
#include <sys/stat.h>
#include "utils.h"
#include "string_utils.h"
#include "linkedList.h"

int foreach_items(state_fs* state, int (*func)(state_fs* stateFs, struct item_head* item));
static int internal_block(state_fs* state, int (*func)(state_fs* stateFs, struct item_head* item));
int copy_by_state(state_fs* stateFs);
static int cd(state_fs** state, const char* path);





char* concat_dirs(char* source_1, char* source_2){
    size_t size = strlen(source_1);
    if(source_1[size-1] == '/')
        return true_concat_strs(source_1, source_2);
    else{
        char *tmp = true_concat_strs(source_1, "/");
        return true_concat_strs(tmp, source_2);
    }
}

state_fs* copyState(state_fs * state){
    state_fs* copy = malloc(sizeof (state_fs));
    copy->fs = state->fs;
    copy->sb = malloc(sizeof (struct reiserfs_super_block));
    memcpy(copy->sb, state->sb, sizeof (struct reiserfs_super_block));
    copy->dir_key = malloc(KEY_SIZE);
    memcpy(copy->dir_key, state->dir_key, KEY_SIZE);
    copy->cache_block = malloc(BLOCK_SIZE);
    memcpy(copy->cache_block, state->cache_block, BLOCK_SIZE);
    copy->dir_name = malloc(strlen(state->dir_name));
    strcpy(copy->dir_name, state->dir_name);

    if(state->dir_names != NULL){
        copy->dir_names = llist_create(state->dir_names->value, state->dir_names->len, state->dir_names->dir_id, state->dir_names->object_id);
    }
    llist_node * iter = state->dir_names->next;
    while(iter != NULL){
        list_add_back(&(copy->dir_names), iter->value, iter->len, iter->dir_id, iter->object_id);
        iter = iter->next;
    }
    if(state -> copyTo != NULL) {
        copy->copyTo = malloc(strlen(state->copyTo));
        strcpy(copy->copyTo, state->copyTo);
    }

    return copy;
}

int init_state(state_fs* state, struct item_head* item){
    if (compare_short_keys(state->dir_key, &(item->ih_key)) == 0 ){
        if(isDir(*item)) {
            struct reiserfs_de_head *dir_entry;
            char *dir_item = state->cache_block + item->ih2_item_location;
            __le16 dir_e_i = 0;
            __le16 name_pos = item->ih2_item_len;
            __le16 prev_pos = name_pos;
            for (int i = 0; i < item->u.ih2_entry_count; i++) {
                dir_entry = dir_item + dir_e_i;
                name_pos = dir_entry->deh2_location;
                char *tmp = malloc(prev_pos - name_pos + 1);
                strncpy(tmp, dir_item + name_pos, prev_pos - name_pos);
                tmp[prev_pos - name_pos] = '\0';
                if (state->dir_names == NULL) {
                    state->dir_names = llist_create(tmp, prev_pos - name_pos + 1, dir_entry->deh2_dir_id,
                                                    dir_entry->deh2_objectid);
                } else {
                    list_add_back(&state->dir_names, tmp, prev_pos - name_pos + 1, dir_entry->deh2_dir_id,
                                  dir_entry->deh2_objectid);
                }
                free(tmp);
                prev_pos = name_pos;
                dir_e_i += DEH_SIZE;
            }
        }
        else if(isStat(*item)){
            struct stat_data *isDir = state->cache_block + item->ih2_item_location;
            __le16 mode = isDir->sd_mode & 0x4000;
            mode = mode >> 12;
            if(mode != 4) return NOT_DIR;
        }
    }
    return 0;
}
int init(state_fs* state){
    llist_free(&(state->dir_names));
    return search(state, init_state);
}
int delete_state(state_fs* stateFs){
    int error=0;
    if(stateFs != NULL){
        if(stateFs->fs != NULL)
            error = fclose(stateFs->fs);
        delete_state_without_close(stateFs);
    }
    return error;
}
int delete_state_without_close(state_fs* stateFs){
    if(stateFs != NULL){
        if(stateFs->dir_key != NULL)
            free(stateFs->dir_key);
        if(stateFs->dir_name != NULL)
            free(stateFs->dir_name);
        if(stateFs->cache_block != NULL)
            free(stateFs->cache_block);
        if(stateFs->dir_names != NULL) {
            llist_free(&(stateFs->dir_names));
            stateFs->dir_names = NULL;
        }
        if(stateFs -> file != NULL){
            fclose(stateFs->file);
            stateFs->file = NULL;
        }
        if(stateFs->copyTo != NULL){
            free(stateFs->copyTo);
            stateFs->copyTo = NULL;
        }
        free(stateFs);
        
    }
    return 0;
}

int create_state(const char* path, state_fs* stateFs){
    //open a partition
    FILE * fs = fopen(path, "rb");
    if (!fs){
        return CANT_OPEN;
    }

    // read a super block
    fseek(fs, REISERFS_DISK_OFFSET_IN_BYTES,SEEK_CUR);
    struct reiserfs_super_block * sb = malloc(sizeof(struct reiserfs_super_block));
    fread(sb,sizeof(struct reiserfs_super_block), 1, fs);

    //is REISER_FS or not
    if(strcmp(sb->s_v1.s_magic, REISERFS_3_6_SUPER_MAGIC_STRING) != 0){
        return ISNT_REISERFS;
    }

    //init root_key
    struct reiserfs_key* root_key = malloc(KEY_SIZE);
    root_key->k2_dir_id = 1;
    root_key->k2_objectid = 2;
    root_key->u.k2_offset_v2.v = 0;

    //init a state
    char *dir_name = malloc(2);
    strcpy(dir_name, "/");
    stateFs->fs = fs;
    stateFs->dir_key = root_key;
    stateFs->sb = sb;
    stateFs->dir_name = dir_name;

    stateFs->dir_names = NULL;
    init(stateFs);
    return 0;
}




int search(state_fs* stateFs, int (*con_s)(state_fs*, struct item_head*)){
    struct reiserfs_super_block* sb = stateFs->sb;
    __le32 rootBlock = sb->s_v1.sb_root_block;
    __le32 off = BLOCK_SIZE * rootBlock;

    int error = fseek(stateFs->fs, off, SEEK_SET);
    char* cache = malloc(BLOCK_SIZE);
    fread(cache, 1, BLOCK_SIZE,stateFs->fs);
    free(stateFs->cache_block);
    stateFs->cache_block = cache;


    return internal_block(stateFs, con_s);

}


int foreach_items(state_fs* state, int (*func)(state_fs* stateFs, struct item_head* item)){
    struct block_head* bhead = state->cache_block;
    struct item_head* item_headers = state->cache_block + BLKH_SIZE;

    for (int i = 0; i < bhead->blk2_nr_item; i++) {
        if(func(state, item_headers+i) == NOT_DIR)
            return NOT_DIR;
    }
    return 0;
}


static int internal_block(state_fs* state, int (*func)(state_fs* stateFs, struct item_head* item)){
    if(state->cache_block == NULL || state->fs == NULL) return NULL_POINTER;
    char* block = malloc(BLOCK_SIZE);
    memcpy(block, state->cache_block, BLOCK_SIZE);
    struct block_head* head = block;
    if(head->blk2_level == 1)
        return foreach_items(state, func);
    else {
        struct reiserfs_key *keys = state->cache_block + BLKH_SIZE;
        int iter = 0;
        while(iter <= head->blk2_nr_item){
            if(iter == head->blk2_nr_item && compare_short_keys(state->dir_key, keys + iter - 1) >= 0) {
                struct disk_child *number_blocks = block + BLKH_SIZE + KEY_SIZE * head->blk2_nr_item;
                __le32 number_block = number_blocks[iter].dc2_block_number;
                fseek(state->fs, BLOCK_SIZE * number_block, SEEK_SET);
                fread(state->cache_block, BLOCK_SIZE, 1, state->fs);
                if(internal_block(state, func) == NOT_DIR)
                    return NOT_DIR;
            }
            else if(compare_short_keys(state->dir_key,keys + iter)<=0){
                struct disk_child *number_blocks = block + BLKH_SIZE + KEY_SIZE * head->blk2_nr_item;
                __le32 number_block = number_blocks[iter].dc2_block_number;
                fseek(state->fs, BLOCK_SIZE * number_block, SEEK_SET);
                fread(state->cache_block, BLOCK_SIZE, 1, state->fs);
                if(internal_block(state, func) == NOT_DIR)
                    return NOT_DIR;
            }
            iter++;
        }
    }
    return 0;
}
int true_cd(state_fs** state, const char* path){
    state_fs * copy = copyState(*state);

    
    
    int relative_cd = cd(&copy, path);
    if(relative_cd != 0){
        copy->dir_key->k2_dir_id = 1;
        copy->dir_key->k2_objectid = 2;
        char *dir_name = malloc(2);
        strcpy(dir_name, "/");
        free(copy->dir_name);
        copy->dir_name = dir_name;
        init(copy);
        int absolute_cd = cd(&copy, path);
        if(absolute_cd == 0){
            delete_state_without_close(*state);
            *state = copy;
            return 0;
        }else{
            delete_state_without_close(copy);
            return absolute_cd;
        }
    }else{
        delete_state_without_close(*state);
        *state = copy;
        return 0;
    }
}



int backside (const char * str){
    if(strcmp(str, "..") != 0){
        return 0;
    }
    return 1;
}

char * without_last_name(char * dirName){
    char * tmp = NULL;
    size_t strlen1 = strlen(strrchr(dirName, '/'));
    size_t strlen2 = strlen(dirName);
    tmp = substring(dirName, 0, strlen2 - strlen1);
    if(strlen(tmp) == 0) {
        tmp = realloc(tmp, 2);
        tmp[0] = '/';
        tmp[1] = '\0';
    }
    return tmp;
}

char * getLastName(char * dirName){
    char * tmp = NULL;
    size_t strlen1 = strlen(strrchr(dirName, '/'));
    size_t strlen2 = strlen(dirName);
    tmp = substring(dirName, strlen2 - strlen1 + 1, strlen2);
}


char * addOrBack(char * dirName, char * pch){
    char * tmp = NULL;
    if(backside(pch) == 1){
        if(strcmp(dirName, "/") != 0){
            tmp = without_last_name(dirName);
        }
    }else{
        tmp = malloc(strlen(dirName) + strlen(pch) + 2);
        strcpy(tmp, dirName);
        if(strcmp(dirName, "/") != 0)
            strcat(tmp, "/");
        strcat(tmp, pch);
    }
    return tmp;
}


static int cd(state_fs** state, const char* path){
    char* trim_path;
    trim(path, &trim_path);
    char * pch = strtok(trim_path, "/");
    state_fs * copy = copyState(*state);


    //init a loop
    struct reiserfs_key* key = find_key((*state)->dir_names, pch);
    if(key == NULL) {
        delete_state_without_close(copy);
        return NOT_FOUND;
    }

    if(key->k2_dir_id == 0 && key->k2_objectid == 1){
        delete_state_without_close(copy);
        free(key);
        return NOT_DIR;
    }
    //name
    char *string = addOrBack(copy->dir_name, pch);
    if(string == NULL) {
        free(key);
        delete_state_without_close(copy);
        return NOT_DIR;
    }
    free(copy->dir_name);
    copy->dir_name = string;

    //key
    free(copy->dir_key);
    copy->dir_key = key;
    int error = init(copy);
    if(error == NOT_DIR) {
        delete_state_without_close(copy);
        return NOT_DIR;
    }
    pch = strtok(NULL, "/");


    while(pch != NULL){
        //find
        struct reiserfs_key* key = find_key(copy->dir_names, pch);
        if(key == NULL){
            delete_state_without_close(copy);
            return NOT_FOUND;
        }

        if(key->k2_dir_id == 0 && key->k2_objectid == 1){
            delete_state_without_close(copy);
            free(key);
            return NOT_DIR;
        }

        //name
        string = addOrBack(copy->dir_name, pch);
        if(string == NULL) {
            delete_state_without_close(copy);
            free(key);
            return NOT_DIR;
        }
        free(copy->dir_name);
        copy->dir_name = string;

        //key
        free(copy->dir_key);
        copy->dir_key = key;
        int copy_error = init(copy);
        if(copy_error == NOT_DIR){
            free(key);
            delete_state_without_close(copy);
            return NOT_DIR;
        }
        pch = strtok(NULL, "/");
    }

    //success
    delete_state_without_close(*state);
    *state = copy;
    return 0;
}


int handleStat(state_fs* state, struct item_head* item, __le16 isDir, __le16 isRegFile, __le64 size_of_file){
    if(isRegFile == 8) {
        char *filename = concat_dirs(state->copyTo, state->dir_name);
        state->file = fopen(filename, "w");
        state->size_of_file = size_of_file;
        free(filename);
    }else if(isDir == 4){
        state_fs * copy = copyState(state);
        char *path = concat_dirs(copy->copyTo, copy->dir_name);
        mkdir(path, 0777);
        init(copy);
        llist_node * iter = copy->dir_names->next->next;
        while(iter != NULL){
            state_fs * copy_item = copyState(copy);
            struct reiserfs_key * keyItem = malloc(KEY_SIZE);
            keyItem->k2_dir_id = iter->dir_id;
            keyItem->k2_objectid = iter->object_id;
            copy_item->dir_key = keyItem;
            copy_item->dir_name = copy_str(iter->value);
            copy_item->copyTo = copy_str(path);
            copy_by_state(copy_item);
            delete_state_without_close(copy_item);
            iter = iter->next;
        }
        free(path);
        delete_state_without_close(copy);
    }
    return 0;
}

enum items get_type(struct item_head* item){
    if(item->ih_format == KEY_FORMAT_1){
        if(item->ih_key.u.k2_offset_v1.k_uniqueness == 0xffffffff)
            return DIRECT;
        else
            return INDIRECT;
    }
    else{
        __le64 type = item->ih_key.u.k2_offset_v2.v >> 60;
        if(type == 2)
            return DIRECT;
        else
            return INDIRECT;
    }
}

void read_block(FILE * disk, __le32 number_block, char* buffer){
    int error = fseek(disk, BLOCK_SIZE * number_block, SEEK_SET);
    int read_buffer = fread(buffer, BLOCK_SIZE, 1, disk);
}

int get_bytes_quantity(int rest_size, int block_size){
    if(rest_size >= block_size)
        return block_size;
    return rest_size;
}

int handle_file_items(state_fs * state, struct item_head * item){
    enum items type = get_type(item);
    switch (type) {
        case DIRECT:{
            int size = get_bytes_quantity(state->size_of_file, item->ih2_item_location);
            state->size_of_file -= size;
            if(state->file != NULL) {
                fwrite(state->cache_block + item->ih2_item_location, 1, size, state->file);
            }
            if(state->size_of_file < 1 && state->file != NULL){
                fclose(state->file);
                state->file = NULL;
            }

            break;
        }
        case INDIRECT:{
            __le32 * number_blocks = state->cache_block + item->ih2_item_location;
            size_t size_mas = item->ih2_item_len / 4 ;
            while (size_mas > 0 && state->size_of_file > 0){
                char * buffer = malloc(BLOCK_SIZE);
                read_block(state->fs, *number_blocks, buffer);


                int size = get_bytes_quantity(state->size_of_file, BLOCK_SIZE);
                if(state->file != NULL) {
                    size_t i = fwrite(buffer, size, 1, state->file);
                    i++;
                }
                state->size_of_file -= size;
                number_blocks += 1;
                size_mas--;
            }
            if(state->size_of_file < 1 && state->file != NULL) {
                fclose(state->file);
                state->file = NULL;
            }
            break;
        }
    }
}

int copy_by_tree(state_fs* state, struct item_head* item){
    if (compare_short_keys(state->dir_key, &(item->ih_key)) == 0){
        if(isStat(*item)){
            if (item->ih_format == KEY_FORMAT_1) {
                struct stat_data_v1* sd = state->cache_block + item->ih2_item_location;
                __le16 isRegFile = (sd->sd_mode & 0x8000) >> 12;
                __le16 isDir = (sd->sd_mode & 0x4000) >> 12;
                __le64 size = sd->sd_size;
                return handleStat(state, item, isDir, isRegFile, size);
            }else{
                struct stat_data* sd = state->cache_block + item->ih2_item_location;
                __le16 isDir = (sd->sd_mode & 0x4000) >> 12;
                __le16 isRegFile = (sd->sd_mode & 0x8000) >> 12;
                __le64 size = sd->sd_size;
                return handleStat(state, item, isDir, isRegFile, size);
            }
        }
        else if(isDir(*item)){
            return 0;
        }
        else{
            __le64 type = item->ih_key.u.k2_offset_v2.v << 4;
            type = type >> 4;
            return handle_file_items(state, item);
        }
    }
}
int copy_by_state(state_fs* stateFs){
    return search(stateFs, copy_by_tree);
}

int copy_by_chars(state_fs* state, char* copied, char* to){
    state_fs * copy = copyState(state);

    if(true_cd(&copy, copied) == 0){
        copy->copyTo = copy_str(to);
        char * last_name = getLastName(copy->dir_name);
        free(copy->dir_name);
        copy->dir_name = last_name;
        int error = copy_by_state(copy);
        delete_state_without_close(copy);
        return error;
    }else{
        if(strrchr(copied, '/') != NULL) {
            char *dir = without_last_name(copied);
            if (true_cd(&copy, dir) == 0) {
                char *last_name = getLastName(copied);
                struct reiserfs_key *key = find_key(copy->dir_names, last_name);
                if (key == NULL) {
                    delete_state_without_close(copy);
                    free(dir);
                    free(last_name);
                    free(key);
                    return NOT_FOUND;
                }
                copy->dir_key = key;
                copy->copyTo = copy_str(to);
                copy->dir_name = copy_str(last_name);
                int error = copy_by_state(copy);
                delete_state_without_close(copy);
                free(dir);
                free(last_name);
                return error;
            } else {
                delete_state_without_close(copy);
                free(dir);
                return NOT_FOUND;
            }
        } else {
            struct reiserfs_key *key = find_key(copy->dir_names, copied);
            if (key == NULL) {
                delete_state_without_close(copy);
                free(key);
                return NOT_FOUND;
            }
            copy->dir_key = key;
            copy->copyTo = copy_str(to);
            copy->dir_name = copy_str(copied);
            int error = copy_by_state(copy);
            delete_state_without_close(copy);
            return error;
        }
    }
}
