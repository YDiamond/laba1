//
// Created by diy on 03.04.2021.
//



#ifndef LABA1_COMMANDS_H
#define LABA1_COMMANDS_H
#include "string.h"
#include "state.h"
#include "linkedList.h"
#include "string_utils.h"

enum code{
    PWD,
    LS,
    CD,
    CP,
    UNKNOWN,
    EXIT
};

struct intercept{
    enum code code;
    char * name;
};

struct args{
    enum code code;
    int size;
};

struct no_arg{
    enum code code;
};

struct arg{
    enum code code;
    char* arg;
};

struct arg2{
    enum code code;
    char * arg;
    char * arg2;
};

union command{
    enum code code;
    struct no_arg noArg;
    struct arg arg;
    struct arg2 arg2;
};


union command get_command(char * line);

const char* pwd(state_fs* stateFs, union command command);

llist_node * ls(state_fs* stateFs, union command command);

int cd(state_fs** stateFs, union command command);

int cp(state_fs* stateFs, union command command);

void free_command(union command* command);


#endif //LABA1_COMMANDS_H
