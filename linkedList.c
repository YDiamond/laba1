//
// Created by YDiamond on 03.11.2020.
//
#include <stdio.h>
#include <malloc.h>
#include "linkedList.h"
#include "module-reiserfs.h"
#include <string.h>

llist_node* llist_create(char *str, int len, __le32 dir_id, __le32 object_id){
    llist_node *tmp = malloc(sizeof(llist_node));
    tmp->prev = NULL;
    tmp->value = malloc(sizeof (char) * len);
    strncpy(tmp->value, str, len);
    tmp->next = NULL;
    tmp->len = len;
    tmp->dir_id = dir_id;
    tmp->object_id = object_id;
    return tmp;
}

llist_node* list_add_front (llist_node** llist, char *str, int len, __le32 dir_id, __le32 object_id){
    llist_node* front = llist_create(str, len, dir_id, object_id);
    if(*llist == NULL){
        *llist = front;
    }
    else{
        front->next = *llist;
        (*llist)->prev = front;
        *llist = front;
    }

    return *llist;
}
llist_node* list_add_back (llist_node** llist, char *str, int len, __le32 dir_id, __le32 object_id){
    llist_node* back = llist_create(str, len, dir_id, object_id);
    if(*llist == NULL){
        *llist = back;
    }
    else{
        llist_node* iter = *llist;
        while (iter->next != NULL) iter = iter->next;
        iter->next = back;
        back->prev = iter;
    }
    return back;
}
llist_node* llist_at(llist_node* llist, int at, int* error){
    if(length(llist) == 0 || at < 0){
        *error = 1;
        return NULL;
    }
    llist_node* iter = llist;
    for(; at > 0 ; --at, iter = iter->next){
        if(iter == NULL){
            *error = 1;
            return NULL;
        }
    }
    return iter;
}
char * list_get (llist_node* list, int index, int* error){
    llist_node* at = llist_at(list, index, error);
    if(*error == 1){
        return 0;
    }
    return at->value;
}

int length(llist_node* llist){
    int length = 0;
    for(;llist != NULL; llist = llist->next)
        length++;
    return length;
}
static void print_str(char *str){
    printf("%s ", str);
}


void llist_free (llist_node** llist){
    while(*llist != NULL){
        llist_node* next = (*llist)->next;
        free((*llist)->value);
        free(*llist);
        *llist = next;
    }

}
void foreach (llist_node* llist, void (*consumer)(char *)){
    for (llist_node* iter = llist ; iter != NULL ; iter = iter->next) {
        consumer(iter->value);
    }
}
struct reiserfs_key* find_key(struct llist_node* llist, const char* value){
    for (llist_node* iter = llist ; iter != NULL ; iter = iter->next) {
        if(strcmp(iter->value,value) == 0){
            struct reiserfs_key* find_key = malloc(KEY_SIZE);
            find_key->k2_dir_id = iter->dir_id;
            find_key->k2_objectid = iter->object_id;
            return find_key;
        }
    }
    return NULL;
}

llist_node * copy(llist_node* llist){

}

//llist_node* map(llist_node* llist, char * (*operator)(char *)){
//    llist_node * iter, *new_list = NULL, * start = NULL;
//
//    for (iter = llist; iter != NULL ; iter = iter->next) {
//        new_list = list_add_back(&new_list, operator(iter->value));
//        if(start == NULL) start = new_list;
//    }
//    return start;
//}
//llist_node *map_mut(llist_node** llist, char * (*operator)(char *)){
//    llist_node *iter;
//    for (iter = *llist; iter != NULL; iter = iter->next){
//        iter->value = operator(iter->value);
//    }
//    return *llist;
//}
//char* foldl(llist_node * llist, char *start_acc, char* (*operator)(char *, char *)){
//    llist_node * iter;
//    int acc = start_acc;
//    for (iter = llist; iter != NULL; iter = iter->next){
//        acc = operator(acc, iter->value);
//    }
//    return acc;
//}


