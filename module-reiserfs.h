//
// Created by diy on 07.03.2021.
//

#ifndef LABA1_MODULE_REISERFS_H
#define LABA1_MODULE_REISERFS_H
#include <stdint.h>
#include <linux/types.h>
#include <linux/reiserfs_fs.h>


/* ReiserFS leaves the first 64k unused, so that partition labels have enough
   space.  If someone wants to write a fancy bootloader that needs more than
   64k, let us know, and this will be increased in size.  This number must be
   larger than than the largest block size on any platform, or code will
   break.  -Hans */
#define REISERFS_DISK_OFFSET_IN_BYTES (64 * 1024)

/* various reiserfs signatures. We have 3 so far. ReIsErFs for 3.5 format with
   standard journal, ReIsEr2Fs for 3.6 (or converted 3.5) and ReIsEr3Fs for
   filesystem with non-standard journal (formats are distinguished by
   sb_version in that case). Those signatures should be looked for at the
   64-th and at the 8-th 1k block of the device */
#define REISERFS_3_5_SUPER_MAGIC_STRING "ReIsErFs"
#define REISERFS_3_6_SUPER_MAGIC_STRING "ReIsEr2Fs"
#define REISERFS_JR_SUPER_MAGIC_STRING "ReIsEr3Fs"	/* JR stands for Journal
                                                        Relocation */

/***************************************************************************/
/*                             SUPER BLOCK                                 */
/***************************************************************************/

struct journal_params {
    __le32 jp_journal_1st_block;	/* where does journal start from on its
					   device */
    __le32 jp_journal_dev;	/* journal device st_rdev */
    __le32 jp_journal_size;	/* size of the journal on FS creation. used to
				   make sure they don't overflow it */
    __le32 jp_journal_trans_max;	/* max number of blocks in a transaction.  */
    __le32 jp_journal_magic;	/* random value made on fs creation (this was
				   sb_journal_block_count) */
    __le32 jp_journal_max_batch;	/* max number of blocks to batch into a trans */
    __le32 jp_journal_max_commit_age;	/* in seconds, how old can an async commit be */
    __le32 jp_journal_max_trans_age;	/* in seconds, how old can a transaction be */
};

enum items{
    DIRECT,
    INDIRECT
};

struct reiserfs_super_block_v1 {
    __le32 sb_block_count;	/* 0 number of block on data device */
    __le32 sb_free_blocks;	/* 4 free blocks count */
    __le32 sb_root_block;	/* 8 root of the tree */

    struct journal_params sb_journal;	/* 12 */

    __le16 sb_blocksize;	/* 44 */
    __le16 sb_oid_maxsize;	/* 46 max size of object id array, see
				   get_objectid() commentary */
    __le16 sb_oid_cursize;	/* 48 current size of object id array */
    __le16 sb_umount_state;	/* 50 this is set to 1 when filesystem was
				   umounted, to 2 - when not */

    char s_magic[10];	/* 52 reiserfs magic string indicates that
				   file system is reiserfs: "ReIsErFs" or
				   "ReIsEr2Fs" or "ReIsEr3Fs" */
    __le16 sb_fs_state;	/* 62 it is set to used by fsck to mark which phase of
				   rebuilding is done (used for fsck debugging) */
    __le32 sb_hash_function_code;	/* 64 code of fuction which was/is/will be
					   used to sort names in a directory. See
					   codes in above */
    __le16 sb_tree_height;	/* 68 height of filesytem tree. Tree
				   consisting of only one root block has 2
				   here */
    __le16 sb_bmap_nr;	/* 70 amount of bitmap blocks needed to
				   address each block of file system */
    __le16 sb_version;	/* 72 this field is only reliable on
				   filesystem with non-standard journal */
    __le16 sb_reserved_for_journal;	/* 74 size in blocks of journal area on
					   main device, we need to keep after
					   non-standard journal relocation */
};



/* Structure of super block on disk */
struct reiserfs_super_block {
/*  0 */ struct reiserfs_super_block_v1 s_v1;
/* 76 */ __le32 sb_inode_generation;
    /* 80 */ __le32 s_flags;
    /* Right now used only by inode-attributes, if enabled */
    /* 84 */ unsigned char s_uuid[16];
    /* filesystem unique identifier */
    /*100 */ char s_label[16];
    /* filesystem volume label */
/*116 */ __le16 s_mnt_count;
/*118 */ __le16 s_max_mnt_count;
/*120 */ __le32 s_lastcheck;
/*124 */ __le32 s_check_interval;
    /*128 */ char s_unused[76];
    /* zero filled by mkreiserfs and reiserfs_convert_objectid_map_v1()
     * so any additions must be updated there as well. */
/*204*/
} __attribute__ ((__packed__));;

#define REISERFS_3_5_SUPER_MAGIC_STRING "ReIsErFs"
#define REISERFS_3_6_SUPER_MAGIC_STRING "ReIsEr2Fs"
#define REISERFS_JR_SUPER_MAGIC_STRING "ReIsEr3Fs"



/***************************************************************************/
/*                       KEY & ITEM HEAD                                   */
/***************************************************************************/
struct offset_v1 {
    __le32 k_offset;
    __le32 k_uniqueness;
} __attribute__ ((__packed__));

/*
 * little endian structure:
 * bits 0-59 [60]: offset
 * bits 60-63 [4]: type
 */
struct offset_v2 {
    __le64 v;
} __attribute__ ((__packed__));

/* Key of the object determines object's location in the tree, composed of 4 components */
struct reiserfs_key {
    __le32 k2_dir_id;	/* packing locality: by default parent directory object id */
    __le32 k2_objectid;	/* object identifier */
    union {
        struct offset_v1 k2_offset_v1;
        struct offset_v2 k2_offset_v2;
    } __attribute__ ((__packed__)) u;
} __attribute__ ((__packed__));

#define KEY_SIZE (sizeof(struct reiserfs_key))

struct pointer{
    __le32 block_number;
    __le16 size;
    __le16 reserved;
}__attribute__ ((__packed__));

#define POINTER_SIZE (sizeof(struct pointer))

#define KEY_FORMAT_1 0
#define KEY_FORMAT_2 1

struct item_head {
    struct reiserfs_key ih_key;	/* Everything in the tree is found by searching for it
					   based on its key. */

    union {
        __le16 ih2_free_space;	/* The free space in the last unformatted node
					   of an indirect item if this is an indirect
					   item.  This equals 0xFFFF iff this is a direct
					   item or stat data item. Note that the key, not
					   this field, is used to determine the item
					   type, and thus which field this union
					   contains. */
        __le16 ih2_entry_count;	/* Iff this is a directory item, this field
					   equals the number of directory entries in
					   the directory item. */
    } __attribute__ ((__packed__)) u;
    __le16 ih2_item_len;	/* total size of the item body */
    __le16 ih2_item_location;	/* an offset to the item body within the
					   block */

    __le16 ih_format;	/* key format is stored in bits 0-11 of this item
				   flags are stored in bits 12-15 */
#if 0
    struct {
		__le16 key_format:12;	/* KEY_FORMAT_1 or KEY_FORMAT_2. This is not
					   necessary, but we have space, let use it */
		__le16 flags:4;	/* fsck set here its flag (reachable/unreachable) */
	} __attribute__ ((__packed__)) ih2_format;
#endif
} __attribute__ ((__packed__));



#define IH_SIZE (sizeof(struct item_head))

/*
 * Picture represents a leaf of internal tree
 *  ______________________________________________________
 * |      |  Array of     |                   |           |
 * |Block |  Object-Item  |      F r e e      |  Objects- |
 * | head |  Headers      |     S p a c e     |   Items   |
 * |______|_______________|___________________|___________|
 */

/* Header of a disk block.  More precisely, header of a formatted leaf
   or internal node, and not the header of an unformatted node. */
struct block_head {
    __le16 blk2_level;	/* Level of a block in the tree. */
    __le16 blk2_nr_item;	/* Number of keys/items in a block. */
    __le16 blk2_free_space;	/* Block free space in bytes. */
    __le16 blk_reserved;
    __le32 reserved[4];
};

#define BLKH_SIZE (sizeof(struct block_head))


#define FREE_LEVEL        0	/* Node of this level is out of the tree. */
#define DISK_LEAF_NODE_LEVEL  1	/* Leaf node level.                       */

/***************************************************************************/
/*                      DIRECTORY STRUCTURE                                */
/***************************************************************************/
/*
   Picture represents the structure of directory items
   ________________________________________________
   |  Array of     |   |     |        |       |   |
   | directory     |N-1| N-2 | ....   |   1st |0th|
   | entry headers |   |     |        |       |   |
   |_______________|___|_____|________|_______|___|
                    <----   directory entries         ------>
 First directory item has k_offset component 1. We store "." and ".."
 in one item, always, we never split "." and ".." into differing
 items.  This makes, among other things, the code for removing
 directories simpler. */
#define SD_OFFSET  0
#define DOT_OFFSET 1
#define DOT_DOT_OFFSET 2

/* Each directory entry has its header. This header has deh_dir_id and
   deh_objectid fields, those are key of object, entry points to */

/* NOT IMPLEMENTED:
   Directory will someday contain stat data of object */
struct reiserfs_de_head {
    __le32 deh2_offset;	/* third component of the directory entry key */
    __le32 deh2_dir_id;	/* objectid of the parent directory of the object,
				   that is referenced by directory entry */
    __le32 deh2_objectid;	/* objectid of the object, that is referenced by
				   directory entry */
    __le16 deh2_location;	/* offset of name in the whole item */
    __le16 deh2_state;	/* whether 1) entry contains stat data (for future),
				   and 2) whether entry is hidden (unlinked) */
} __attribute__ ((__packed__));

#define DEH_SIZE sizeof(struct reiserfs_de_head)


/***************************************************************************/
/*                             STAT DATA                                   */
/***************************************************************************/

/* Stat Data on disk (reiserfs version of UFS disk inode minus the address blocks) */

/* The sense of adding union to stat data is to keep a value of real number of
   blocks used by file.  The necessity of adding such information is caused by
   existing of files with holes.  Reiserfs should keep number of used blocks
   for file, but not calculate it from file size (that is not correct for
   holed files). Thus we have to add additional information to stat data.
   When we have a device special file, there is no need to get number of used
   blocks for them, and, accordingly, we doesn't need to keep major and minor
   numbers for regular files, which might have holes. So this field is being
   overloaded.  */

struct stat_data_v1 {
    __le16 sd_mode;		/* file type, permissions */
    __le16 sd_nlink;		/* number of hard links */
    __le16 sd_uid;		/* owner */
    __le16 sd_gid;		/* group */
    __le32 sd_size;		/* file size */
    __le32 sd_atime;		/* time of last access */
    __le32 sd_mtime;		/* time file was last modified  */
    __le32 sd_ctime;		/* time inode (stat data) was last changed (except
				   changes to sd_atime and sd_mtime) */
    union {
        __le32 sd_rdev;
        __le32 sd_blocks;	/* number of blocks file uses */
    } __attribute__ ((__packed__)) u;
    __le32 sd_first_direct_byte;	/* first byte of file which is stored
					   in a direct item: except that if it
					   equals 1 it is a symlink and if it
					   equals MAX_KEY_OFFSET there is no
					   direct item.  The existence of this
					   field really grates on me. Let's
					   replace it with a macro based on
					   sd_size and our tail suppression
					   policy.  Someday.  -Hans */
} __attribute__ ((__packed__));

/* Stat Data on disk (reiserfs version of UFS disk inode minus the
   address blocks) */
struct stat_data {
    __le16 sd_mode;		/* file type, permissions */
    __le16 sd_attrs;
    __le32 sd_nlink;		/* 32 bit nlink! */
    __le64 sd_size;		/* 64 bit size! */
    __le32 sd_uid;		/* 32 bit uid! */
    __le32 sd_gid;		/* 32 bit gid! */
    __le32 sd_atime;		/* time of last access */
    __le32 sd_mtime;		/* time file was last modified  */
    __le32 sd_ctime;		/* time inode (stat data) was last changed (except
				   changes to sd_atime and sd_mtime) */
    __le32 sd_blocks;
    union {
        __le32 sd_rdev;
        __le32 sd_generation;
        //__le32 sd_first_direct_byte;
        /* first byte of file which is stored in a direct item: except that if
           it equals 1 it is a symlink and if it equals ~(__le32)0 there is no
           direct item.  The existence of this field really grates on me. Let's
           replace it with a macro based on sd_size and our tail suppression
           policy? */
    } __attribute__ ((__packed__)) u;
} __attribute__ ((__packed__));

/***************************************************************************/
/*                      DISK CHILD                                         */
/***************************************************************************/
/* Disk child pointer: The pointer from an internal node of the tree
   to a node that is on disk. */
struct disk_child {
    __le32 dc2_block_number;	/* Disk child's block number. */
    __le16 dc2_size;		/* Disk child's used space.   */
    __le16 dc2_reserved;
} __attribute__ ((__packed__));

/***************************************************************************/
/*                      FUNCTIONS                                         */
/***************************************************************************/
int compare_short_keys(struct reiserfs_key const* a, struct reiserfs_key const* b);
int compare_long_keys(struct reiserfs_key const* a, struct reiserfs_key const* b);



#endif //LABA1_MODULE_REISERFS_H
