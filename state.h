//
// Created by diy on 03.04.2021.
//

#include <stdio.h>
#include "module-reiserfs.h"
#include "linkedList.h"

#ifndef LABA1_STATE_H
#define LABA1_STATE_H

#define NULL_POINTER -1
#define STOP 1
#define CONTINUE 0
#define FOUND 11
#define NOT_FOUND -11
#define NOT_DIR -666

#define CANT_OPEN -1
#define ISNT_REISERFS -2
#define BLOCK_SIZE 4096

#define NOT_INTERNAL_BLOCK -3




typedef struct {
    FILE* fs;
    struct reiserfs_super_block* sb;
    struct reiserfs_key* dir_key;
    char* cache_block;
    llist_node* dir_names;
    char* dir_name;

    //info handling
    __le64 size_of_file;
    FILE* file;
    char* copyTo;
}state_fs;

int create_state(const char* path, state_fs* stateFs);

int search(state_fs* stateFs, int (*con_s)(state_fs*, struct item_head* ));

int init(state_fs* state);

int delete_state(state_fs* stateFs);

int true_cd(state_fs** state, const char* path);

int copy_by_chars(state_fs* state, char* copied, char* to);

int delete_state_without_close(state_fs* stateFs);
#endif //LABA1_STATE_H
