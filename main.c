#define __USE_GNU
#include <stdio.h>
#include <malloc.h>
#include <fcntl.h>
#include <unistd.h>
#include "module-reiserfs.h"
#include "state.h"
#include "commands.h"
#include "linkedList.h"
#include "string_utils.h"
#include "partition.h"

typedef enum {false, true} Bool;

#include "user_interface.h"
#include "mode.h"


int main(int argc, char* argv[]) {

    struct program args = get_args(argc, argv);
    if (args.mode == UNDEFINED){
        printf("Incorrect arguments of the programme.\n");
        printf("\t-m - mode of the programme. 1 - Partitions. 2 - ReiserFs3 filesystem.\n");
        printf("\t-p - path to the device.\n");
    }else if (args.mode == PARTITIONS){
        print_partitions();
    }else if (args.mode == FS){
        if (entrance(args.path) == 1) printf("Произошла ошибка.\n");
    }
    return 0;
}

