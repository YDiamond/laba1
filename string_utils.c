//
// Created by diy on 09.05.2021.
//

#include "string_utils.h"
#include <string.h>
#include <malloc.h>

char * copy_str(char* str){
    int size = strlen(str);
    char * copy = malloc(size + 1);
    strcpy(copy, str);
    return copy;
}

int is_ws(char c){
    return c == ' ' || c == '\n';
}
int trim(char* src, char ** dst){
    if(strlen(src) == 0)
        return 0;

    size_t src_size = strlen(src);
    int lp = 0;
    int rp = 0;
    while(is_ws(src[lp]) && ++lp);
    while(is_ws(src[src_size-rp-1]) && ++rp);

    (*dst) = (char*)malloc(sizeof(char) * src_size-lp-rp+1);

    for(int i = lp; i < src_size-rp; ++i)
        (*dst)[i-lp] = src[i];

    (*dst)[src_size-lp-rp] = '\0';
    return lp+rp;

}

char * substring(const char * str, int begin, int end){
    if(strlen(str) < end && begin < 0 && begin > end)
        return NULL;

    int size = end - begin + 1;
    char * tmp = malloc(size);
    strncpy(tmp, str + begin, size-1);
    tmp[size - 1] = '\0';
    return tmp;
}


int start_with(char* src, char* substr) {
    if (strncmp(src, substr, strlen(substr)) == 0) return 1;
    return 0;
}

void reverse(char* str, int len)
{
    int i = 0, j = len - 1, temp;
    while (i < j) {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++;
        j--;
    }
}


char* true_concat_strs(char* source_1, char* source_2){
    char* dest = malloc(strlen(source_1) + strlen(source_2) + 1);
    strcpy(dest, source_1);
    strcat(dest, source_2);
    return dest;
}