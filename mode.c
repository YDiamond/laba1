//
// Created by diy on 25.08.2021.
//
#include "mode.h"
const char* options = "m:p:";
struct program get_args(const int argc, char** argv) {
    struct  program args;
    int opt;
    args.mode = UNDEFINED;
    while ((opt = getopt(argc, argv, options)) != -1) {
        switch (opt) {
            case 'm':
                if (strcmp(optarg, "=1") == 0) {
                    args.mode = PARTITIONS;
                }else if (strcmp(optarg, "=2") == 0){
                    args.mode = FS;
                }
                break;
            case 'p':
                args.path = optarg;
                args.path ++;
                break;
        }
    }
    return args;
}
